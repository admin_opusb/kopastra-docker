#FROM alpine:latest
FROM registry.opusb.co.id/idempieremaster:latest

ENV IDEMPIERE_VERSION idempiere-server.zip

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
ENV IDEMPIERE_HOME /opt/idempiere-server

WORKDIR $IDEMPIERE_HOME

#RUN apk update && \
#    apk --no-cache add tzdata openjdk8 bash dos2unix libc6-compat unzip msttcorefonts-installer && \
#    update-ms-fonts && \
#    fc-cache -f && \
#    rm -rf /var/cache/apk/*

COPY $IDEMPIERE_VERSION /tmp/$IDEMPIERE_VERSION 

RUN unzip -q -o /tmp/$IDEMPIERE_VERSION -d /opt && \
    rm -rf /tmp/$IDEMPIERE_VERSION

COPY docker-entrypoint.sh $IDEMPIERE_HOME
COPY idempiere-server.sh $IDEMPIERE_HOME
COPY MANIFEST.MF $IDEMPIERE_HOME
COPY key /root/key
COPY logo_kopastra /$IDEMPIERE_HOME/logo_kopastra

RUN chmod +x $IDEMPIERE_HOME/docker-entrypoint.sh && \
    chmod +x $IDEMPIERE_HOME/idempiere-server.sh

RUN dos2unix $IDEMPIERE_HOME/*.sh && \
    ln -s $IDEMPIERE_HOME/idempiere-server.sh /usr/bin/idempiere && \
    rm -rf /tmp/* 

ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["idempiere"]
