#!/usr/bin/env bash

set -Eeo pipefail

echo "iDempiere App Kopastra Setup"

IDEMPIERE_WEBUI=$(ls -ltr /opt/idempiere-server/plugins/ | grep "org.adempiere.ui.zk" | awk '{print $9}')
KEY_STORE_PASS=${KEY_STORE_PASS:-myPassword}
KEY_STORE_ON=${KEY_STORE_ON:-idempiere.org}
KEY_STORE_OU=${KEY_STORE_OU:-iDempiere Docker}
KEY_STORE_O=${KEY_STORE_O:-iDempiere}
KEY_STORE_L=${KEY_STORE_L:-myTown}
KEY_STORE_S=${KEY_STORE_S:-CA}
KEY_STORE_C=${KEY_STORE_C:-US}
IDEMPIERE_HOST=${IDEMPIERE_HOST:-0.0.0.0}
IDEMPIERE_PORT=${IDEMPIERE_PORT:-8080}
IDEMPIERE_SSL_PORT=${IDEMPIERE_SSL_PORT:-8443}
DB_HOST=${DB_HOST:-postgres}
DB_PORT=${DB_PORT:-5432}
DB_NAME=${DB_NAME:-idempiere}
DB_USER=${DB_USER:-adempiere}
DB_PASS=${DB_PASS:-adempiere}
DB_ADMIN_PASS=${DB_ADMIN_PASS:-postgres}
MAIL_HOST=${MAIL_HOST:-0.0.0.0}
MAIL_USER=${MAIL_USER:-info}
MAIL_PASS=${MAIL_PASS:-info}
MAIL_ADMIN=${MAIL_ADMIN:-info@idempiere}
MIGRATE_EXISTING_DATABASE=${MIGRATE_EXISTING_DATABASE:false}
WEBUI=${WEBUI:-webui}
XMS=${XMS:-Xms64M}
XMX=${XMX:-Xmx512M}
IPHOSTS=${IPHOSTS:-127.0.0.1}
IPDMN=${IPDMN:-kopastradev-cubejs.opusb.co.id}
USERJMX=${USERJMX:-admin}
PASSJMX=${PASSJMX:-admin}

if [[ -n "$DB_PASS_FILE" ]]; then
    echo "DB_PASS_FILE set as $DB_PASS_FILE..."
    DB_PASS=$(cat $DB_PASS_FILE)
fi

if [[ -n "$DB_ADMIN_PASS_FILE" ]]; then
    echo "DB_ADMIN_PASS_FILE set as $DB_ADMIN_PASS_FILE..."
    DB_ADMIN_PASS=$(cat $DB_ADMIN_PASS_FILE)
fi

if [[ -n "$MAIL_PASS_FILE" ]]; then
    echo "MAIL_PASS_FILE set as $MAIL_PASS_FILE..."
    MAIL_PASS=$(cat $MAIL_PASS_FILE)
fi

if [[ -n "$KEY_STORE_PASS_FILE" ]]; then
    echo "KEY_STORE_PASS_FILE set as $KEY_STORE_PASS_FILE..."
    KEY_STORE_PASS=$(cat $KEY_STORE_PASS_FILE)
fi

if [[ "$1" == "idempiere" ]]; then
    RETRIES=30

    until PGPASSWORD=$DB_ADMIN_PASS psql -h $DB_HOST -p $DB_PORT -U postgres -c "\q" && PGPASSWORD=$DB_PASS psql -h $DB_HOST -p $DB_PORT -U $DB_USER -d $DB_NAME -c "\q" > /dev/null 2>&1 || [[ $RETRIES == 0 ]]; do
        echo "Can't connect to server postgresql, $((RETRIES--)) remaining attempts..."
        sleep 1
    done

    if [[ $RETRIES == 0 ]]; then
        echo "Shutting down..."
        exit 1
    fi

#    echo -e "$JAVA_HOME\n$IDEMPIERE_HOME\n$KEY_STORE_PASS\n$KEY_STORE_ON\n$KEY_STORE_OU\n$KEY_STORE_O\n$KEY_STORE_L\n$KEY_STORE_S\n$KEY_STORE_C\n$IDEMPIERE_HOST\n$IDEMPIERE_PORT\n$IDEMPIERE_SSL_PORT\nN\n2\n$DB_HOST\n$DB_PORT\n$DB_NAME\n$DB_USER\n$DB_PASS\n$DB_ADMIN_PASS\n$MAIL_HOST\n$MAIL_USER\n$MAIL_PASS\n$MAIL_ADMIN\nY\n" | ./console-setup.sh
 
    echo -e "$JAVA_HOME\n$IDEMPIERE_HOME\n$KEY_STORE_PASS\n$IDEMPIERE_HOST\n$IDEMPIERE_PORT\n$IDEMPIERE_SSL_PORT\nY\n2\n$DB_HOST\n$DB_PORT\n$DB_NAME\n$DB_USER\n$DB_PASS\n$DB_ADMIN_PASS\n$MAIL_HOST\n$MAIL_USER\n$MAIL_PASS\n$MAIL_ADMIN\nY\n" | ./console-setup.sh

    echo "$IPHOSTS      $IPDMN" >> /etc/hosts

	echo ===================================
	echo Config WEBUI
	echo ===================================

	sed -i 's/TEST/'${WEBUI}'/g' $IDEMPIERE_HOME/MANIFEST.MF
	cp -avr $IDEMPIERE_HOME/MANIFEST.MF $IDEMPIERE_HOME/plugins/$IDEMPIERE_WEBUI/META-INF/MANIFEST.MF
	cp -avr $IDEMPIERE_HOME/logo_kopastra/*.png $IDEMPIERE_HOME/plugins/$IDEMPIERE_WEBUI/theme/default/images
	cp -avr $IDEMPIERE_HOME/logo_kopastra/*.png $IDEMPIERE_HOME/plugins/$IDEMPIERE_WEBUI/theme/columbian-blue/images
	cp -avr $IDEMPIERE_HOME/logo_kopastra/*.png $IDEMPIERE_HOME/plugins/$IDEMPIERE_WEBUI/theme/violet/images
	#sed -i 's/TEST/'${WEBUI}'/g' $IDEMPIERE_HOME/plugins/$IDEMPIERE_WEBUI/META-INF/MANIFEST.MF

	echo ===================================
	echo Change Memory Java
	echo ===================================

	sed -i 's/Xms64M/'${XMS}'/g' $IDEMPIERE_HOME/idempiereEnv.properties
	sed -i 's/Xmx512M/'${XMX}'/g' $IDEMPIERE_HOME/idempiereEnv.properties
	sed -i 's/Xms64M/'${XMS}'/g' $IDEMPIERE_HOME/utils/myEnvironment.sh
	sed -i 's/Xmx512M/'${XMX}'/g' $IDEMPIERE_HOME/utils/myEnvironment.sh
	
	echo ==================================
	echo JMX Security
	echo ==================================

	echo "${USERJMX} ${PASSJMX}" > $IDEMPIERE_HOME/jmxremote.password
	echo "${USERJMX} readwrite" > $IDEMPIERE_HOME/jmxremote.access

	chmod 600 $IDEMPIERE_HOME/jmxremote.password
	chmod 600 $IDEMPIERE_HOME/jmxremote.access
fi

exec "$@"
