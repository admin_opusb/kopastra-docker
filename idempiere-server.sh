#!/bin/sh
#

unset DISPLAY
BASE=`dirname $( readlink -f $0 )`
. $BASE/utils/myEnvironment.sh Server
if [ $JAVA_HOME ]; then
  JAVA=$JAVA_HOME/bin/java
else
  JAVA=java
  echo JAVA_HOME is not set.
  echo You may not be able to start the server
  echo Set JAVA_HOME to the directory of your local JDK.
fi

if [ "$1" = "debug" ]; then
  DEBUG="-Xdebug -Xnoagent -Xrunjdwp:transport=dt_socket,address=4554,server=y,suspend=n"
fi

echo ===================================
echo Starting iDempiere Server
echo ===================================

VMOPTS="-Dorg.osgi.framework.bootdelegation=sun.security.ssl
-Dorg.osgi.framework.bootdelegation=org.netbeans.lib.profiler,org.netbeans.lib.profiler.* 
-Dcom.sun.management.jmxremote=true
-Dcom.sun.management.jmxremote.port=8070
-Dcom.sun.management.jmxremote.authenticate=true 
-Dcom.sun.management.jmxremote.ssl=false
-Dcom.sun.management.jmxremote.password.file=$IDEMPIERE_HOME/jmxremote.password
-Dcom.sun.management.jmxremote.access.file=$IDEMPIERE_HOME/jmxremote.access
-Djetty.home=$BASE/jettyhome
-Djetty.base=$BASE/jettyhome
-Djetty.etc.config.urls=etc/jetty.xml,etc/jetty-deployer.xml,etc/jetty-ssl.xml,etc/jetty-ssl-context.xml,etc/jetty-http.xml,etc/jetty-https.xml
-Dosgi.console=localhost:12612
-Dmail.mime.encodefilename=true
-Dmail.mime.decodefilename=true
-Dmail.mime.encodeparameters=true
-Dmail.mime.decodeparameters=true"

# BEGIN ANSIBLE MANAGED BLOCK
#$JAVA ${DEBUG} $IDEMPIERE_JAVA_OPTIONS -verbose:gc -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -XX:ParallelGCThreads=20 -XX:ConcGCThreads=4 -XX:InitiatingHeapOccupancyPercent=70 -XX:-HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=$IDEMPIERE_HOME/log/java.hprof -XX:+PrintGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:$IDEMPIERE_HOME/log/java.log $VMOPTS -jar $BASE/plugins/org.eclipse.equinox.launcher_1.*.jar -application org.adempiere.server.application > /dev/null 2>&1 & echo $! > $IDEMPIERE_HOME/app.pid
# END ANSIBLE MANAGED BLOCK

$JAVA ${DEBUG} $IDEMPIERE_JAVA_OPTIONS -verbose:gc -XX:+UseG1GC -XX:MaxGCPauseMillis=${MaxGCPauseMillis} -XX:ParallelGCThreads=${ParallelGCThreads} -XX:ConcGCThreads=${ConcGCThreads} -XX:InitiatingHeapOccupancyPercent=${InitiatingHeapOccupancyPercent} -XX:+UnlockExperimentalVMOptions -XX:G1NewSizePercent=${G1NewSizePercent} -XX:G1MaxNewSizePercent=${G1MaxNewSizePercent} -XX:G1ReservePercent=${G1ReservePercent} -XX:+DisableExplicitGC -XX:+PrintGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:$IDEMPIERE_HOME/log/java.log $VMOPTS -jar $BASE/plugins/org.eclipse.equinox.launcher_1.*.jar -application org.adempiere.server.application

